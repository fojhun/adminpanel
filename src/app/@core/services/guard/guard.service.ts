import { AuthService } from "./../auth/auth.service";
import {
    ActivatedRouteSnapshot,
    CanActivate,
    Router,
    RouterStateSnapshot,
} from "@angular/router";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root",
})
export class GuardService implements CanActivate {
    constructor(private router: Router, private authenticationService: AuthService) {}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        const tokenValue = this.authenticationService.tokenValue;
        if(currentUser || tokenValue){
            return true;
        }
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
