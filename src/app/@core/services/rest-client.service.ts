
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { environment } from "../../../environments/environment";
import { catchError, timeout } from "rxjs/operators";

import { NotificationService } from "./notification.service";
import { AuthService } from "./auth/auth.service";

@Injectable({
    providedIn: "root",
})
export class RestClientService {
    constructor(

        private http: HttpClient,
        private auth: AuthService,
        private notiService: NotificationService
    ) {}

    static makeHttpHeaders() {
        const token = localStorage.get("token")
            ? localStorage.get("token")
            : "";
        return new HttpHeaders({
            Authorization: `Bearer ${token}`,
        });
    }
    readonly BASE_URL: string = environment.server_address;

    public get(url: string): Observable<any> {
        return this.restHandler("get", this.BASE_URL + url);
    }

    public post(url: string, body: any): Observable<any> {
        const options = { body };
        return this.restHandler("post", this.BASE_URL + url, options);
    }

    public put(url: string, body: any): Observable<any> {
        const options = { body };
        return this.restHandler("put", this.BASE_URL + url, options);
    }

    public delete(url: string, body?: any): Observable<any> {
        const options = { body };
        return this.restHandler("delete", url, options);
    }

    private restHandler(requestType, url, options?) {
        const theOptions = {
            headers: RestClientService.makeHttpHeaders(),
            body: options ? (options.body ? options.body : null) : null,
        };

        return this.http
            .request(requestType, this.BASE_URL + url, theOptions)
            .pipe(
                catchError((err) => {
                    if (err.status === 401) {
                        this.auth.logout();
                        this.notiService.showError("You are logged out!");
                    } else {
                        this.notiService.showError(
                            err.error.errorCode || err.message
                        );
                    }
                    return throwError(err);
                }),
                timeout(60000)
            );
    }
}
