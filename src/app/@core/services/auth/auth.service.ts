import { environment } from "./../../../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, of } from "rxjs";
import { catchError, map } from "rxjs/operators";


@Injectable({
    providedIn: "root",
})
export class AuthService {
    public currentUserSubject: BehaviorSubject<any>;
    public tokenSubject: BehaviorSubject<any> = new BehaviorSubject(true);
    readonly BASE_URL: string = environment.server_address;

    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }
    public get tokenValue(): any {
        return this.tokenSubject.value;
    }
    constructor(
        private http: HttpClient,

    ) {}
    public login(body) {
        return this.http
            .post(`${this.BASE_URL}/Users/authenticate`, body)
            .pipe(
                map((user) => {
                    console.log(user, "hhhhhhh");
                    // if (user ? !!user.token : false && !user.nextStepNeeded) {
                    //   localStorage.setItem('token', user.token);
                    //   this.currentUserSubject.next(user);
                    // }

                    // return user;
                }),
                catchError((err) => of(`${err}`))
            );
    }
    logout() {
        this.currentUserSubject.next(false);
        this.tokenSubject.next(false);
    }
}
