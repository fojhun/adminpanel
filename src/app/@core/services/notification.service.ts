import { Injectable } from "@angular/core";
import { MessageService } from "primeng/api";

@Injectable({
    providedIn: "root",
})
export class NotificationService {
    constructor(private messageService: MessageService) {}

    public showError(message) {
        this.messageService.add({ severity: "error", detail: message });
    }

    public showSuccess(message) {
        this.messageService.add({ severity: "success", detail: message });
    }
    public showWarning(message) {
        this.messageService.add({ severity: "warning", detail: message });
    }
}
