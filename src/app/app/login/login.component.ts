import { NotificationService } from "./../../@core/services/notification.service";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "src/app/@core/services/auth/auth.service";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    constructor(
        private auth: AuthService,
        private fb: FormBuilder,
        private notiService: NotificationService
    ) {}

    ngOnInit() {
        this.initForm();
    }
    initForm(): void {
        this.loginForm = this.fb.group({
            password: ["", Validators.required],
            username: ["", Validators.required],
        });
    }
    onSubmit() {
        let body = {
            id: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            username: this.loginForm.get('username').value,
            password: this.loginForm.get('password').value,
            token: "",
        };
        console.log(
            this.loginForm.value,
            body,
            "this.loginForm.valuethis.loginForm.value"
        );
        this.auth.login(body).subscribe((data) => {
            console.log(data, "gaggagagag");
            // if (data && data) {

            //             this.notiService.showSuccess("Login successful!");

            //     this.notiService.showWarning("Wrong username or password!");
        });
    }
}
