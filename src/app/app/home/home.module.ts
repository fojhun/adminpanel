import { NgModule } from "@angular/core";
import {
    CommonModule,
    HashLocationStrategy,
    LocationStrategy,
} from "@angular/common";
import {
    AutoCompleteModule,
    OverlayPanelModule,
    PanelModule,
    ScrollPanelModule,
} from "primeng/primeng";
import { HeaderModule } from "src/app/components/header/header.module";
import { AppBreadcrumbModule } from "src/app/components/app-breadcrumb/app-breadcrumb.module";
import { ProfileModule } from "src/app/components/profile/profile.module";
import { NavigationModule } from "src/app/components/navigation/navigation.module";
import { AppRoutes } from "src/app/app.routes";
import { HomeComponent } from "./home.component";
import {ToastModule} from 'primeng/toast';
@NgModule({
    declarations: [HomeComponent],
    imports: [
        CommonModule,
        AutoCompleteModule,
        OverlayPanelModule,
        PanelModule,
        HeaderModule,
        AppBreadcrumbModule,
        ProfileModule,
        NavigationModule,
        ScrollPanelModule,
        ToastModule,
        AppRoutes,
    ],
    exports: [HomeComponent],
    providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
})
export class HomeModule {}
