import { HttpClientModule } from "@angular/common/http";
import { LoginModule } from "./app/login/login.module";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppComponent } from "./app.component";
import { HomeModule } from "./app/home/home.module";
import { AuthService } from "./@core/services/auth/auth.service";
import { GuardService } from "./@core/services/guard/guard.service";
import { FormsModule } from "@angular/forms";
import { MessageService } from "primeng/api";

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HomeModule,
        LoginModule,
        HttpClientModule,
        FormsModule,
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    providers: [GuardService, AuthService, MessageService],
})
export class AppModule {}
