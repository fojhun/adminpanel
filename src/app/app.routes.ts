
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { GuardService } from './@core/services/guard/guard.service';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./app/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'panel',
    canActivate: [GuardService],
    loadChildren: () => import('./app/home/home.module').then(m => m.HomeModule)
  },
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

  export class AppRoutes {}
