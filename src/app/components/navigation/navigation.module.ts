import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MegaMenuModule } from "primeng/megamenu";
import { MenuModule } from "primeng/menu";
import { MenubarModule } from "primeng/menubar";
import { PanelMenuModule } from "primeng/panelmenu";
import { AppMenuComponent, AppSubMenuComponent } from "./app.menu.component";
@NgModule({
    declarations: [AppMenuComponent, AppSubMenuComponent],
    imports: [
        CommonModule,
        MegaMenuModule,
        MenuModule,
        MenubarModule,
        PanelMenuModule,
    ],
    exports: [AppMenuComponent],
})
export class NavigationModule {}
