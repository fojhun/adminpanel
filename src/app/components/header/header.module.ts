import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HeaderComponent } from "./header.component";
import { ToolbarModule } from 'primeng/toolbar';
@NgModule({
    declarations: [HeaderComponent],
    imports: [CommonModule, ToolbarModule,],
    exports: [HeaderComponent],
})
export class HeaderModule {}
