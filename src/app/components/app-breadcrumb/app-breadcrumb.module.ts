import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppBreadcrumbComponent } from './app-breadcrumb.component';
import { BreadcrumbService } from './breadcrumb.service';
import { BreadcrumbModule } from 'primeng/primeng';

@NgModule({
  declarations: [AppBreadcrumbComponent],
  imports: [CommonModule,BreadcrumbModule],
  providers: [BreadcrumbService],
  exports: [AppBreadcrumbComponent],
})
export class AppBreadcrumbModule { }
